package com.blitterhead.juanita

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton


class ActivityMain : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById<View>(R.id.toolbar) as Toolbar)

        (findViewById<View>(R.id.fab) as FloatingActionButton).apply {
            setOnClickListener { navigateToBitBucket() }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun navigateToBitBucket() {
        startActivity(Intent(Intent.ACTION_VIEW).apply {
            setData(Uri.parse("https://bitbucket.org/mveroukis/juanita"))
        })
    }
}
