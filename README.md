# Juanita #


### What is it? ###

A simple Android app that displays information about the device based entirely on configuration qualifiers.

![Juanita Screenshot](https://lh3.googleusercontent.com/qvBmmHIx50zCnLleKhGM5On8CZrJjUdlJ42gL-3LTjP3X17ZmWT_8ZaGe2efadzf6w)

### Why does it exist? ###

There are so many different devices and it's often not obvious how each device is configured. Android uses configuration qualifiers for it's resources, which means you can use different sets of drawables depending on the screen density or orientation. Sometimes it's not obvious what a particular device's ***screen size*** or ***screen pixel density*** is, but with this app it is.

The app hardly has any code, all the logic is done by the Android system. String resources have been created for most resource configuration qualifiers (ex: values-ldpi, values-large, etc) and those string values are displayed in a table. When Android loads the app it loads the appropriate resource based on the device configuration. In fact, the app works even in the Android Studio layout editor preview because it doesn't need any code to display the configuration data.

To learn more about Android configuration qualifiers, see the official docs: [Providing Resources](https://developer.android.com/guide/topics/resources/providing-resources.html)

Currently, the app handles the following configurations:

  1. Platform version
  1. Size
  1. Density
  1. Orientation
  1. Ratio
  1. Round screen
  1. Layout direction
  1. UI Mode
  1. Night mode
  1. Touchscreen type
  1. Keyboard availability
  1. Primary text input
  1. Primary navigation

It does not currently handle parameterized configuration qualifiers like ***smallest width*** and ***available width*** because there's a very large number of possibilities and I have no intention of creating that many string resources. One exception is the ***platform version*** because the number of qualifiers is reasonably small and I felt it was worth it.

### How do I get set up? ###

Simple, just clone it and open it with Android Studio.


### Contribution guidelines ###

So far there's not much going on, but if you want to contribute send me pull request. Not sure what else to say, this is my first open source project.

[![Google Play Badge](https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png =256x "Google Play and the Google Play logo are trademarks of Google Inc.")](https://play.google.com/store/apps/details?id=com.blitterhead.juanita)
